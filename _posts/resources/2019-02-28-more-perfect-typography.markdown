---
layout: resource
title:  "More Perfect Typography"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Tim Brown
link: https://vimeo.com/17079380
description: At long last, designers can use real fonts on the web. But what now? Where do we go from here?
categories: 
  - Resources 
  - Basics
tags:
  - Talk
  - Video
---

{{ page.author }}
