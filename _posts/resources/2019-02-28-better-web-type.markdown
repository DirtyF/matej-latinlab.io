---
layout: resource
title:  "Better Web Type"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Matej Latin
link: /web-typography-course/
description: A free web typography course for web designers & web developers.
categories: 
  - Resources 
  - Basics
tags:
  - Course
  - Frontend
  - Sketch
---

{{ page.author }}
