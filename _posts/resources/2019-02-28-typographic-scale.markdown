---
layout: resource
title:  "Typographic Scale"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Henryk Wollik
link: https://github.com/automat/sketch-plugin-typographic-scale
description: This plugin generates a typographic scale from selected text layers.
categories: 
  - Resources 
  - Visual-Hierarchy
tags:
  - Plugin
  - Sketch
---

{{ page.author }}
