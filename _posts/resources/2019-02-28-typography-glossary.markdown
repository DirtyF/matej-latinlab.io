---
layout: resource
title:  "Typography Glossary"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: FontShop
link: https://www.fontshop.com/glossary
description: A glossary of common type terminology.
categories: 
  - Resources 
  - Typeface-Anatomy
tags:
  - Glossary
---

{{ page.author }}
