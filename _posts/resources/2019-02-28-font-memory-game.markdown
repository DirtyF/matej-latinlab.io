---
layout: resource
title:  "Font Memory Game"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Matej Latin
link: /font-memory-game/
description: Recognizing different styles of fonts is one of the most challenging parts of typography. This game helps you train your eyes to notice the smallest details.
image: /assets/img/resources/bwt-fmg@2x.jpg
categories: 
  - Resources 
  - Better-Web-Type
tags:
  - Game
---

{{ page.author }}
