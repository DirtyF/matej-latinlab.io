---
layout: resource
title:  "Equilateral Triangle of a Perfect Paragraph"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Matej Latin
link: /triangle/
description: A learning game that teaches players how to shape typographically perfect paragraphs.
image: /assets/img/resources/bwt-triangle@2x.jpg
categories: 
  - Resources 
  - Better-Web-Type
tags:
  - Game
---

{{ page.author }}
