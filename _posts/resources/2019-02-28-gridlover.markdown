---
layout: resource
title:  "Gridlover"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Tuomas Jomppanen & Ville V. Vanninen
link: https://www.gridlover.net/
description: Establish a typographic system with modular scale & vertical rhythm.
categories: 
  - Resources 
  - Vertical-Rhythm
  - Visual-Hierarchy
tags:
  - Tool
  - Frontend
---

{{ page.author }}
