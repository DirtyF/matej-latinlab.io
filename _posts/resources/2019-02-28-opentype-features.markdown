---
layout: resource
title:  "Normalize OpenType"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Kenneth Ormandy
link: https://github.com/kennethormandy/normalize-opentype.css
description: Adds OpenType features—ligatures, kerning, and more—to Normalize.css.
categories: 
  - Resources 
  - OpenType
tags:
  - Tool
  - Frontend
---

{{ page.author }}
