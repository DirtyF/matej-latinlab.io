---
layout: resource
title:  "Typeset.js"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: David Merfield
link: https://blot.im/typeset/
description: A ty­po­graphic pre-proces­sor for your html which uses zero client-side JavaScript and gives you advanced typographic features.
categories: 
  - Resources 
  - OpenType
tags:
  - Tool
  - Frontend
---

{{ page.author }}
