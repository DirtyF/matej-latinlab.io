---
layout: resource
title:  "Modular Scale"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Scott Kellum and Tim Brown
link: http://www.modularscale.com/
description: The Modular Scale calculator multiplies your scale’s base with a ratio to produce a scale of numbers that are proportionally related.
categories: 
  - Resources 
  - Visual-Hierarchy
tags:
  - Tool
  - Frontend
---

{{ page.author }}
