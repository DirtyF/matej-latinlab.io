---
layout: resource
title:  "FontBase"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Dominik Levitsky Studio
link: https://fontba.se/insteadof/fontcase
description: FontBase is a free, beautifully crafted font utility for designers, that gives you all the possibilities of Fontcase.
categories: 
  - Resources 
  - Font-Tools
tags:
  - App
---

{{ page.author }}
