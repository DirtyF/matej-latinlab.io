---
layout: page
title: Privacy Policy
permalink: /privacy-policy/
exclude: true
---

<h2 class="h3">No bullshit privacy policy</h2>

I take privacy very seriously. I hate it when my personal data is used without my consent and possibly against my best interests. I want you to know that your personal data is safe. I use Mailchimp for sending email campaigns and Shopify for processing payments. Both are very secure and transparent about data collection. I would never sell data of my subscribers to anyone, the same as I don't want to ruin the website with advertisement. I only want that the visitors of my website have fun and learn about web typography. If you have any concerns or wish your data to be deleted, [contact me](/contact/).

—Matej

<h2 class="h3">Legal privacy policy mumbo jumbo</h2>

Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.

* Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.
* We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.
* We will only retain personal information as long as necessary for the fulfillment of those purposes.
* We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.
* Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.
* We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.
* We will make readily available to customers information about our policies and practices relating to the management of personal information.

We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.
