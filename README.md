# Better Web Type

This is a public project for the Open Source website/blog [Better Web Type](https://betterwebtype.com/). It uses Jekyll to build a static website and Forestry to manage its content. If you have an idea for a blog post, feel free to [open a new issue](https://gitlab.com/matej-latin/matej-latin.gitlab.io/issues) and select the `BWT-blog-post` template.

## Assigning issue weights

| Weights | Description |
|---------|-------------|
| 1 | Minor changes or updates, very little coding involved, no questions/problems to resolve |
| 2 | Small change, design updates, coding may be involved, no major questions/problems to resolve |
| 3 | Medium changes or updates, coding may be involved, may be questions/problems to resolve |
| 5 | Producing new content/features, coding may be involved, questions/problems to resolve |
| 8 | Producing content/major features, coding may be involved, significant questions/problems to resolve |
| 13 | Huge features and new products, major coding may be involved, major questions/problems to resolve |

