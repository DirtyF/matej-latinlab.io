---
layout: post
categories:
- Articles
tags:
- Vertical-Rhythm
- Baseline-Grid
- Sketch
- Design-Systems
date: 2019-05-15 06:30:09 +0000
author: matej
toc: true
comments: true
title: Designing With a Baseline Grid in Sketch
intro: A simple guide to designing with a baseline grid
description: A simple guide to designing with a baseline grid
image: ''
title-maxwidth: ''

---
A simple guide to designing with a baseline grid