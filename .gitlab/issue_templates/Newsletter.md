**Send date: [mmm dd, yyyy]**

### News

- Donations
- News item

### Featured

- Featured article

### Font of the month

- Font

### New resources

- Resource

### Articles

- Articles

### Did you know?

- Interesting fact about typography

---

**Content ideas:**

- inspiration/gallery
- featured top donor
- cool new fonts
- added new resources
- quick tips
- feature book reviews
- sketch tips

**Sources:**

- https://www.typemag.org/
- [variable font articles at the bottom](https://blog.prototypr.io/an-exploration-of-variable-fonts-37f85a91a048)
- https://fonts.google.com/specimen/IBM+Plex+Sans

/label ~Newsletter ~Campaign ~Promo